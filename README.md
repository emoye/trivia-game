# trivia-game

An online trivia game.

Heroku link to the trivia game: https://cryptic-hollows-85142.herokuapp.com/

## Description

This online trivia game is a Single Page Application built using the Vue.js framework (version 2.6.14). The game consists of a start screen, questions screen and result screen. The application uses an API to store users and another API to generate questions for the quiz.

## Installation

Write the following in the terminal to install node modules:

```
npm install
```

## Run the application

Write the following in the terminal to run the application:

```
npm run serve
```

## Start Screen

The trivia game app starts on the Start Screen. Here you must input your desired username before you can choose what kind of quiz you would like. You can choose number of questions, with a minimum of 1 question and a maximum of 50 questions. NB: Not all categories have 50 questions, so you might experience getting less than 50 questions even though you choose 50. Next, you can choose the difficulty of the questions. You can choose between easy, medium and hard. You can also choose a specific category. NB: You must choose number of questions you want in the quiz. If you do not wish to choose difficulty and category, the default difficulty is easy and the default category is General Knowledge. When you are satisified with the number of questions, difficulty and category, you can click "Start Quiz!".

## Questions Screen

The "Start Quiz!" button takes you to the Question Screen where each question will be displayed one at a time. Click on the answer you think is correct and the next question will be displayed. When you answer the last question you will be taken to the Result Screen. Each correctly answered question should count 10 points.

## Result Screen

The Result Screen will show you all the questions and answer alternatives. The right answer will be green. If you answered incorrectly, your answer will be red. Your achieved score out of the total possible score will be displayed. If you managed to get a new highscore (good job!), the new highscore will be updated and saved to the database. If you click the "Play again" button you will be taken back to the Question Screen and play the quiz again with the same number of questions, difficulty and category (but with new questions). If you want to go back to the start screen, click "Home".

## User API

The user API stores a user object in json format. The application can fetch and verify if a username exists or not, and fetch and update highscore.

## Quiz API

The following API was used to generate question for the Trivia Game app: https://opentdb.com/api_config.php. The user inputs (number of questions, difficulty and category) are used to generate questions. The questions are returned in the form of an array that contains question objects.
