const apiURL = 'https://noroff-assignment-api-emoye.herokuapp.com';
const apiKey = '9gBLjmloJSGc4aXYAgCecUmt4RZL1GOGy40wEQwCtQZzMXlgk8ZIWnaBNrOSvwCn';

export const userInfo = {

  /**
   * Method that takes in an object containing a username (details).
   * Returns the user with the given username if user.ok is true.
   * If not, an error is thrown 
   */
  async login(details) {
    try {
      const user = await fetch(`${apiURL}/trivia?username=${details.username}`);

      if (!user.ok) {
        throw new Error('User not found');
      }
      return await user.json();
    } 
    catch (error) {
      console.error(error);
    }
  },

  /**
   * Method to create a new user in the database.
   * The method takes in an object containing a username (details).
   * Cheks if user.ok is true, if not an error is thrown 
   */
  async createUser(details) {
    const requestOptions = {
      method: 'POST',
      headers: {
        'X-API-Key': apiKey,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: details.username,
        highScore: 0,
      }),
    };

    try {
      const user = await fetch(`${apiURL}/trivia`, requestOptions);

      if (!user.ok) {
        throw new Error('Could not create user');
      }
      return await user.json();
    } 
    catch (error) {
      console.error(error);
    }
  },

  /**
   * Method to update a users highscore in the API.
   * The method takes in a user id (userId) which is the id that comes from the user that is logged in.
   * The method also takes in the quiz score (quizScore) which is the new highscore that is saved to the logged in user in the database
   */
  async updateHighscore(userId, quizScore) {
    const requestOptions = {
      method: 'PATCH',
      headers: {
        'X-API-Key': apiKey,
        'Content-Type': 'application/json',
      },

      body: JSON.stringify({
        highScore: quizScore
      }),
    };

    try {
      const response = await fetch(`${apiURL}/trivia/${userId}`, requestOptions);

      if (!response.ok) {
        throw new Error('Highscore could not be updated :^(');
      }
    }
    catch (error) {
      console.error(error);
    }
  }
};