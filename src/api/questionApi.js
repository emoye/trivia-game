const baseURL = 'https://opentdb.com/api';

export const questionApi = {
    /** 
     * Method that gets quiz categories from the Quiz API.
     * If successful, the method returns an array with error set to null and the quiz categories from the API.
     * If an error occurs, the method returns an array with an error message and an empty array 
     */
    async getCategories() {
        try {
            const categories = await fetch(`${baseURL}_category.php`)
            const { trivia_categories } = await categories.json();

            return [null, trivia_categories];
        }
        catch (error) {
            return [error.message, []];
        }
    },

    /** 
     * Method to get quiz questions from the quiz API.
     * The method takes in the quiz info inputted by the user, which is an URLSearchParam object (quizInfo).
     * If successful, the method returns an array with error set to null and the quiz questions from the API.
     * If an error occurs, the method returns an array with an error message and an empty array 
     */
    async getQuestions(quizInfo) {
        try {
            const response = await fetch(`${baseURL}.php?${quizInfo.toString()}`);
            const result = await response.json();

            return [null, result];
        }
        catch (error) {
            return [error.message, []];
        }
    },

    /**
     * Method to get quiz token from the quiz API.
     * If successful, the method returns an array with error set to null and the token from the API.
     * If an error occurs, the method return an array with an error message and an empty array 
     */
    async getToken() {
        try {
            const response = await fetch(`${baseURL}_token.php?command=request`);
            const result = await response.json();
            
            return [null, result];
        }
        catch (error) {
            return [error.message, []];
        }
    }
};